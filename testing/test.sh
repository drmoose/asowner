#!/bin/bash


if [ "$#" -lt 4 ]; then
	exec >&2
	echo "Usage: $0 [runtime] [base_image] [owner_uid] [runtime_uid] [mode] ...run-args"
	echo
	echo "Where:"
	echo "  [runtime] is 'docker' or 'podman'"
	echo
	echo "  [base_image] is the FROM line of the dockerfile, e.g. debian:sid"
	echo
	exit 2
fi

runtime="$1"; shift
base_image="$1"; shift
owner_uid="$1"; shift
runtime_uid="$1"; shift
mode="${1:-match}"; shift
HERE="$(dirname "$0")"
cd "$HERE/.."
REPO_ROOT="$(pwd)"

export DOCKER_SCAN_SUGGEST=false

match() {
	test r"$(stat -c %u "$workdir")" = r"$(stat -c %u "$workdir/target")"
	test r"$(stat -c %g "$workdir")" = r"$(stat -c %g "$workdir/target")"
}

perform_test() {
	$mode
	test_result="$?"
	{ set +x ; } &>/dev/null
	return "$test_result"
}

set -xeo pipefail
iidfile=$(mktemp)
workdir=$(mktemp -d)
chmod 777 "$workdir"

# docker-as-sudo
docker run --rm -iv "$(dirname "$workdir")":/wp:Z alpine chown "$owner_uid" "/wp/$(basename "$workdir")"

{ echo "FROM $base_image" ; ./as_owner_of.sh --dockerfile /work; } | $runtime build --iidfile=$iidfile -f - "$REPO_ROOT"
$runtime run --rm -i -v "$workdir":/work:Z -u "$runtime_uid" "$@" "$(<$iidfile)" touch /work/target
if perform_test; then
	echo "Success!"
else
	echo "FAIL"
	exit 1
fi
