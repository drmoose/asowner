# as_owner_of.sh

This file runs code as the owner of the given folder, creating a matching
user account if necessary.  We need to do this on Docker Linux (but not
Docker Mac or Podman) because mapped folders don't get UID mapping, so
editing files within a mapped folder from a docker container can cause them
to be owned by root.

**Supported distros:**
- debian
- ubuntu
- fedora
- redhat variants with dnf or yum
- alpine
- arch

**Usage:**
- `./as_owner_of.sh --dockerfile [mapped drive]` <br/>
  Emit Dockerfile code for installing this script. The [mapped drive] is a
  path to a VOLUME you expect to be mounted by the calling user.

- `./as_owner_of.sh --setup` <br/>
  Invoke from a dockerfile to install dependencies. Must be run as root.

- `./as_owner_of.sh [filename] [cmd argv...]` <br/>
  Invoke the given command argv as the user+group that owns the given file.
  This should be the ENTRYPOINT of your docker container.

**Environment Variables at `docker run` time:**
- `FAKE_ACCOUNT` <br/>
  Name of the user/group to add if needed to match permissions. Defaults
  to "hostuser"

- `FORCE_ROOT` <br/>
  When set to "yes", will prevent this script from doing anything, even if
  the container is running as root and the mapped drive is owned by someone
  else.

# License
This repo Copyright (c) 2022-2023 drmoose.net and is distributed under the terms
of the BSD license. Please distribute it widely. Copy & paste it into your
own source code. Claim it's yours. I don't care. I just want your docker
containers to stop trashing the permissions on my system.
